# DownloadTab
Currently Just a clone of https://addons.mozilla.org/en-US/firefox/addon/download-tab/

I'm planning on expanding on this addon to also include functionality to put history, bookmarks, and tags, in a tab (so one will never need to open firefox's library).
