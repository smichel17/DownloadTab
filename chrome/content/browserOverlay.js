var downloadtab = {
	load: function() {
		var initial = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefService)
							 .getBranch("extensions.downloadtab.").getBoolPref('initial');
		if (initial) {
			var toolbar = document.getElementById('nav-bar');
			if (!toolbar.currentSet.match('downloadtab-24')) {
					var newset = toolbar.currentSet.concat(',downloadtab-24');
					toolbar.currentSet = newset;
					toolbar.setAttribute('currentset', newset);
					document.persist(toolbar.id, "currentset");
			}
			Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefService)
							 .getBranch("extensions.downloadtab.").setBoolPref('initial', false);
		}
	},
	clck: function() {
		var k=gBrowser.addTab("chrome://mozapps/content/downloads/downloads.xul");
		gBrowser.selectedTab=k;
	}	
}
window.addEventListener("load",function(event){downloadtab.load();},false);